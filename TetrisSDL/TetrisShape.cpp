#include "TetrisShape.h"
#include "Input.h"

TetrisShape::TetrisShape(TetrisShapeType shapeType, TetrisBoard* board)
{
	m_board = board;
	m_rotationIndex = 0;

	m_layout = new TetrisShapeLayout(shapeType);
	m_color = m_layout->GetColour();
	
	int initialWidth = m_layout->GetWidth(m_rotationIndex);
	int initialHeight = m_layout->GetHeight(m_rotationIndex);
	m_offsetX = initialWidth / 2;
	m_offsetY = initialHeight / 2;

	m_blocks = new std::vector<TetrisBlock*>();
	int numberOfBlocks = m_layout->GetNumberOfBlocks();
	for (int blockIndex = 0; blockIndex < numberOfBlocks; blockIndex++)
	{
		m_blocks->push_back(new TetrisBlock(m_color));
	}

	int startingX = (TetrisBoard::c_width - 1) / 2 + (TetrisBoard::c_width - 1) % 2;
	SetPosition(startingX, initialHeight / 2);
}

TetrisShape::~TetrisShape()
{
	if (m_alive)
	{
		for (TetrisBlock *tb : *m_blocks)
		{
			delete tb;
		}
	}
	m_blocks->clear();
	delete m_blocks;
	delete m_layout;
}

bool TetrisShape::IsAlive()
{
	return m_alive;
}

void TetrisShape::Update()
{
	if (Input::KeyWasPressed(SDLK_a))
	{
		bool canMove = true;
		for (TetrisBlock *tb : *m_blocks)
		{
			int newX = tb->GetPositionX() - 1;
			canMove &= m_board->IsValidPosition(newX, tb->GetPositionY());
		}

		if (canMove)
		{
			SetPosition(GetPositionX() - 1, GetPositionY());
		}
	}
	else if (Input::KeyWasPressed(SDLK_d))
	{
		bool canMove = true;
		for (TetrisBlock *tb : *m_blocks)
		{
			int newX = tb->GetPositionX() + 1;
			canMove &= m_board->IsValidPosition(newX, tb->GetPositionY());
		}

		if (canMove)
		{
			SetPosition(GetPositionX() + 1, GetPositionY());
		}
	}
	else if (Input::KeyWasPressed(SDLK_r))
	{
		Rotate();
	}
}

void TetrisShape::Rotate()
{
	int newRotation = m_rotationIndex + 1;
	if (newRotation >= m_layout->GetNumberOfRotations())
	{
		newRotation = 0;
	}

	int numberOfBlocks = m_layout->GetNumberOfBlocks();

	std::vector<TetrisBlockOffset> *offsets = m_layout->GetBlockOffsets(newRotation);

	bool isValidPosition = true;
	for (int blockIndex = 0; blockIndex < numberOfBlocks; blockIndex++)
	{
		TetrisBlockOffset tbo = offsets->at(blockIndex);

		int newX = m_xPosition + tbo.m_offsetX;
		int newY = m_yPosition + tbo.m_offsetY;
		isValidPosition &= m_board->IsValidPosition(newX, newY);
	}

	if (isValidPosition)
	{
		m_rotationIndex = newRotation;
		SetPosition(GetPositionX(), GetPositionY());
	}
}


void TetrisShape::MoveDown()
{
	bool canDrop = true;
	for (TetrisBlock *tb : *m_blocks)
	{
		int newY = tb->GetPositionY() + 1;
		canDrop &= m_board->IsValidPosition(tb->GetPositionX(), newY);
	}

	if (canDrop)
	{
		SetPosition(GetPositionX(), GetPositionY() + 1);
	}
	else
	{
		for (TetrisBlock *tb : *m_blocks)
		{
			m_board->SetBlockAt(tb->GetPositionX(), tb->GetPositionY(), tb);
		}
		m_board->TryClearRows();
	}

	m_alive = canDrop;
}

void TetrisShape::Render(SDL_Renderer *renderer)
{
	for (TetrisBlock *tb : *m_blocks)
	{
		tb->Draw(renderer);
	}
}

void TetrisShape::SetPosition(int x, int y)
{
	m_xPosition = x - m_offsetX;
	m_yPosition = y - m_offsetY;
	
	int numberOfBlocks = m_layout->GetNumberOfBlocks();
	std::vector<TetrisBlockOffset> *blockOffsets = m_layout->GetBlockOffsets(m_rotationIndex);
	for (int blockIndex = 0; blockIndex < numberOfBlocks; blockIndex++)
	{
		TetrisBlock *tb = m_blocks->at(blockIndex);
		TetrisBlockOffset tbo = blockOffsets->at(blockIndex);

		tb->SetPosition(m_xPosition + tbo.m_offsetX, m_yPosition + tbo.m_offsetY);
	}
}

int TetrisShape::GetPositionX()
{
	return m_xPosition + m_offsetX;
}

int TetrisShape::GetPositionY()
{
	return m_yPosition + m_offsetY;
}
