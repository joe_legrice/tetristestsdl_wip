#include <algorithm>
#include "Input.h"
#include <iostream>

std::vector<SDL_Keycode> Input::m_keysDown;
std::vector<SDL_Keycode> Input::m_keysPressedThisFrame;

Input::Input()
{
	m_quitEvent = false;
}


Input::~Input()
{
}

bool Input::QuitEventRaised()
{
	return m_quitEvent;
}

void Input::Update()
{
	for (auto &key : m_keysDown)
	{
		m_keysPressedThisFrame.erase(std::remove(m_keysPressedThisFrame.begin(), m_keysPressedThisFrame.end(), key), m_keysPressedThisFrame.end());
	}

	SDL_Event sdlEvent;
	while (SDL_PollEvent(&sdlEvent))
	{
		m_quitEvent |= sdlEvent.type == SDL_QUIT;
		if (sdlEvent.type == SDL_KEYDOWN)
		{
			SDL_Keycode keyCode = sdlEvent.key.keysym.sym;
			if (std::find(m_keysDown.begin(), m_keysDown.end(), keyCode) == m_keysDown.end())
			{
				m_keysPressedThisFrame.push_back(sdlEvent.key.keysym.sym);
				m_keysDown.push_back(sdlEvent.key.keysym.sym);
			}
		}
		else if (sdlEvent.type == SDL_KEYUP)
		{
			m_keysDown.erase(std::remove(m_keysDown.begin(), m_keysDown.end(), sdlEvent.key.keysym.sym), m_keysDown.end());
		}
	}
}

bool Input::KeyWasPressed(SDL_Keycode key)
{
	return std::find(m_keysPressedThisFrame.begin(), m_keysPressedThisFrame.end(), key) != m_keysPressedThisFrame.end();
}

bool Input::KeyIsPressed(SDL_Keycode key)
{
	return std::find(m_keysDown.begin(), m_keysDown.end(), key) != m_keysDown.end();
}
