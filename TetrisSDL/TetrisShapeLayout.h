#pragma once

#include <vector>
#include "TetrisBlock.h"
#include "json.hpp"

using JSON = nlohmann::json;

enum TetrisShapeType
{
	kTetrisShapeLine,
	kTetrisShapeSquare,
	kTetrisShapeL,
	kTetrisShapeReverseL,
	kTetrisShapeT,
	kTetrisShapeDiamond,
	kTetrisShapeReverseDiamond,
	kTetrisShapeLast
};

struct TetrisBlockOffset
{
	int m_offsetX;
	int m_offsetY;
};

class TetrisShapeLayout
{
public:
	TetrisShapeLayout(TetrisShapeType);
	~TetrisShapeLayout();

	SDL_Color GetColour();
	std::vector<TetrisBlockOffset>* GetBlockOffsets(int rotationIndex);

	int GetWidth(int rotationIndex);
	int GetHeight(int rotationIndex);

	int GetNumberOfBlocks();
	int GetNumberOfRotations();

private:
	static JSON s_tetrisShapeModel;
	SDL_Color m_colour;
	std::vector<std::vector<TetrisBlockOffset>*>* m_layouts;

	void Populate(std::string);
};
