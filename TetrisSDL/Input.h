#pragma once
#include <vector>
#include <SDL.h>

class Input
{
public:
	Input();
	~Input();

	bool QuitEventRaised();
	void Update();
	static bool KeyWasPressed(SDL_Keycode key);
	static bool KeyIsPressed(SDL_Keycode key);

private:
	bool m_quitEvent;

	static std::vector<SDL_Keycode> m_keysDown;
	static std::vector<SDL_Keycode> m_keysPressedThisFrame;
};
