#include "TetrisGame.h"
#include "Input.h"
#include "Time.h"
#include <time.h>


TetrisGame::TetrisGame()
{
	m_board = new TetrisBoard();
	m_currentShape = nullptr;
	m_dropTimeMillis = 1000;
	m_dropProgressMillis = 0;
	SpawnShape();
}

TetrisGame::~TetrisGame()
{	
	delete m_board;
	m_board = nullptr;

	if (m_currentShape != nullptr)
	{
		delete m_currentShape;
		m_currentShape = nullptr;
	}
}

void TetrisGame::Update()
{
	if (Input::KeyIsPressed(SDLK_s))
	{
		m_dropTimeMillis = 100;
	}
	else
	{
		m_dropTimeMillis = 1000;
	}

	m_dropProgressMillis += Time::fixedUpdateStepMillis;
	if (m_dropProgressMillis > m_dropTimeMillis)
	{
		m_dropProgressMillis = 0;
		m_currentShape->MoveDown();
		
		if (!m_currentShape->IsAlive())
		{
			SpawnShape();
		}
	}

	m_currentShape->Update();
}

void TetrisGame::Render(SDL_Renderer *renderer)
{
	m_currentShape->Render(renderer);
	m_board->Render(renderer);
}

void TetrisGame::SpawnShape()
{
	if (m_currentShape != nullptr)
	{
		delete m_currentShape;
	}

	srand(time(NULL));
	TetrisShapeType tst = static_cast<TetrisShapeType>(rand() % kTetrisShapeLast);
	m_currentShape = new TetrisShape(tst, m_board);
}
