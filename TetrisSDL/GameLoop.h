#pragma once

#include <memory>
#include <SDL.h>

#include "Input.h"
#include "TetrisGame.h"

class GameLoop
{
public:
	GameLoop(SDL_Renderer *renderer);
	~GameLoop();
	void Run();

private:
	SDL_Renderer *m_renderer;
	Input *m_input;
	TetrisGame *m_tetrisGame;
};

