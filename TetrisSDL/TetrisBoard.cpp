#include "TetrisBoard.h"
#include "GameWindow.h"

TetrisBoard::TetrisBoard()
{
	for (int i = 0; i < c_width * c_height; i++)
	{
		m_allBlocks[i] = nullptr;
	}
	GameWindow::SetWindowSize(c_width * TetrisBlock::c_blockSize, c_height * TetrisBlock::c_blockSize);
}

TetrisBoard::~TetrisBoard()
{
	for (TetrisBlock *tb : m_allBlocks)
	{
		delete tb;
	}
}

void TetrisBoard::SetBlockAt(unsigned int x, unsigned int y, TetrisBlock* b)
{
	unsigned int index = y * c_width + x;
	if (index < c_width * c_height)
	{
		if (m_allBlocks[index] != nullptr)
		{
			delete m_allBlocks[index];
		}
		m_allBlocks[index] = b;
	}
}

void TetrisBoard::TryClearRows()
{
	for (int rowIndex = c_height - 1; rowIndex >= 0; rowIndex--)
	{
		bool allClear = true;
		for (int columnIndex = 0; columnIndex < c_width; columnIndex++)
		{ 
			allClear &= m_allBlocks[rowIndex * c_width + columnIndex] != nullptr;
			if (!allClear)
			{
				break;
			}
		}

		if (allClear)
		{
			for (int columnIndex = 0; columnIndex < c_width; columnIndex++)
			{
				delete m_allBlocks[rowIndex * c_width + columnIndex];
				m_allBlocks[rowIndex * c_width + columnIndex] = nullptr;
			}

			for (int rowIndexShifted = rowIndex - 1; rowIndexShifted >= 0; rowIndexShifted--)
			{
				for (int columnIndex = 0; columnIndex < c_width; columnIndex++)
				{
					TetrisBlock *blockToMove = m_allBlocks[rowIndexShifted * c_width + columnIndex];
					m_allBlocks[rowIndexShifted * c_width + columnIndex] = nullptr;

					int newRow = rowIndexShifted + 1;
					m_allBlocks[newRow * c_width + columnIndex] = blockToMove;
					if (blockToMove != nullptr)
					{
						blockToMove->SetPosition(columnIndex, newRow);
					}
				}
			}

			rowIndex++;
		}
	}
}

void TetrisBoard::Render(SDL_Renderer *renderer)
{
	for (TetrisBlock *tb : m_allBlocks)
	{
		if (tb != nullptr)
		{
			tb->Draw(renderer);
		}
	}
}

bool TetrisBoard::IsValidPosition(unsigned int x, unsigned int y)
{
	if (x >= 0 && x < c_width && y >= 0 && y < c_height)
	{
		return m_allBlocks[y * c_width + x] == nullptr;
	}
	else
	{
		return false;
	}
}
