#pragma once

#include <SDL.h>
#include "TetrisShape.h"

class TetrisGame
{
public:
	TetrisGame();
	~TetrisGame();
	
	void Update();
	void Render(SDL_Renderer *renderer);

private:
	int m_dropTimeMillis;
	int m_dropProgressMillis;
	TetrisShape *m_currentShape;
	TetrisBoard *m_board;
	
	void SpawnShape();
};
