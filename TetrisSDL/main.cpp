#include <iostream>
#include <SDL.h>

#include "GameWindow.h"
#include "GameLoop.h"

int main(int argc, char** argv)
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		std::cout << "SDL_INIT Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_DisplayMode current;
	if (SDL_GetCurrentDisplayMode(0, &current) != 0)
	{
		std::cout << "SDL_GetCurrentDisplayMode Error:" << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	int width = 480;
	int height = 640;
	int screenX = (int)(0.5f * (current.w - width));
	int screenY = (int)(0.5f * (current.h - height));
	SDL_Window *window = SDL_CreateWindow("Tetris", screenX, screenY, width, height, SDL_WINDOW_SHOWN);
	if (window == nullptr)
	{
		SDL_Quit();
		return 1;
	}
	
	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (renderer == nullptr)
	{
		std::cout << "SDL_CreateRenderer Error:" << SDL_GetError() << std::endl;
		SDL_DestroyWindow(window);
		SDL_Quit();
		return 1;
	}

	GameWindow *gameWindow = new GameWindow(window);
	GameLoop *gameLoop = new GameLoop(renderer);
	gameLoop->Run();

	delete gameWindow;
	delete gameLoop;

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}