#ifndef TETRISBLOCK_H
#define TETRISBLOCK_H

#include <SDL.h>

class TetrisBlock
{
public:
	static const int c_blockSize = 32;

	TetrisBlock::TetrisBlock(SDL_Color color);
	void Draw(SDL_Renderer *renderer);
	void SetPosition(int x, int y);
	int GetPositionX();
	int GetPositionY();
	
private:
	int m_xPosition;
	int m_yPosition;
	SDL_Color m_color;
};

#endif
