#include "GameWindow.h"

SDL_Window* GameWindow::s_window;

GameWindow::GameWindow(SDL_Window *window)
{
	s_window = window;
}


GameWindow::~GameWindow()
{
}

void GameWindow::SetWindowSize(int w, int h)
{
	SDL_SetWindowSize(s_window, w, h);
}
