#include "GameLoop.h"
#include "TetrisGame.h"
#include "Time.h"

GameLoop::GameLoop(SDL_Renderer *renderer)
{
	m_renderer = renderer;
	m_input = new Input();
	m_tetrisGame = new TetrisGame();
}


GameLoop::~GameLoop()
{
	delete m_input;
	delete m_tetrisGame;
}


void GameLoop::Run()
{
	bool didQuit = false;

	unsigned int millis_lag = 0;
	unsigned int millis_last = SDL_GetTicks();
	while (!didQuit)
	{
		unsigned int millis_current = SDL_GetTicks();
		unsigned int millis_elapsed = millis_current - millis_last;
		millis_last = millis_current;
		millis_lag += millis_elapsed;

		m_input->Update();
		didQuit |= m_input->QuitEventRaised() || Input::KeyWasPressed(SDLK_q);

		while (millis_lag >= Time::fixedUpdateStepMillis)
		{
			m_tetrisGame->Update();
			millis_lag -= Time::fixedUpdateStepMillis;
		}

		SDL_SetRenderDrawColor(m_renderer, 255, 255, 255, 255);
		SDL_RenderClear(m_renderer);
		m_tetrisGame->Render(m_renderer);
		SDL_RenderPresent(m_renderer);
	}
}
