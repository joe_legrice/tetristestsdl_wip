#include "TetrisShapeLayout.h"
#include <fstream>

JSON TetrisShapeLayout::s_tetrisShapeModel;

void TetrisShapeLayout::Populate(std::string shapeId)
{
	JSON myObject = s_tetrisShapeModel[shapeId];
	
	m_colour = SDL_Color();
	m_colour.r = myObject["Colour"]["r"];
	m_colour.g = myObject["Colour"]["g"];
	m_colour.b = myObject["Colour"]["b"];
	m_colour.a = myObject["Colour"]["a"];

	m_layouts = new std::vector<std::vector<TetrisBlockOffset>*>();
	JSON myLayouts = myObject["Layouts"];
	for (unsigned int i = 0; i < myLayouts.size(); i++)
	{
		JSON thisLayout = myLayouts.at(i);
		std::vector<TetrisBlockOffset> *thisLayoutVector = new std::vector<TetrisBlockOffset>();
		m_layouts->push_back(thisLayoutVector);

		for (unsigned int blockIndex = 0; blockIndex < thisLayout.size(); blockIndex++)
		{
			TetrisBlockOffset tbo;
			tbo.m_offsetX = thisLayout.at(blockIndex)["x"];
			tbo.m_offsetY = thisLayout.at(blockIndex)["y"];

			thisLayoutVector->push_back(tbo);
		}
	}
}

TetrisShapeLayout::TetrisShapeLayout(TetrisShapeType tst)
{
	if (s_tetrisShapeModel == nullptr)
	{
		std::ifstream tetrisShapeDataFile("ShapeData.json");
		if (tetrisShapeDataFile.is_open())
		{
			tetrisShapeDataFile >> s_tetrisShapeModel;
			tetrisShapeDataFile.close();
		}
	}

	switch (tst)
	{
		case kTetrisShapeLine:
			Populate("TetrisShapeLine");
			break;
		case kTetrisShapeSquare:
			Populate("TetrisShapeSquare"); 
			break;
		case kTetrisShapeL:
			Populate("TetrisShapeL");
			break;
		case kTetrisShapeReverseL:
			Populate("TetrisShapeReverseL");
			break;
		case kTetrisShapeT:
			Populate("TetrisShapeT");
			break;
		case kTetrisShapeDiamond:
			Populate("TetrisShapeDiamond");
			break;
		case kTetrisShapeReverseDiamond:
			Populate("TetrisShapeReverseDiamond");
			break;
		case kTetrisShapeLast:
		default:
			break;
	}
}



TetrisShapeLayout::~TetrisShapeLayout()
{
	for (std::vector<TetrisBlockOffset> *tboList : *m_layouts)
	{
		delete tboList;
	}
	delete m_layouts;
}

SDL_Color TetrisShapeLayout::GetColour()
{
	return m_colour;
}

std::vector<TetrisBlockOffset>* TetrisShapeLayout::GetBlockOffsets(int rotationIndex)
{
	return m_layouts->at(rotationIndex);
}

int TetrisShapeLayout::GetWidth(int rotationIndex)
{
	std::vector<TetrisBlockOffset> *currentLayout = GetBlockOffsets(rotationIndex);

	int maxX = 0;
	for (TetrisBlockOffset tbo : *currentLayout)
	{
		if (maxX < tbo.m_offsetX)
		{
			maxX = tbo.m_offsetX;
		}
	}
	return maxX + 1;
}

int TetrisShapeLayout::GetHeight(int rotationIndex)
{
	std::vector<TetrisBlockOffset> *currentLayout = GetBlockOffsets(rotationIndex);

	int maxY = 0;
	for (TetrisBlockOffset tbo : *currentLayout)
	{
		if (maxY < tbo.m_offsetY)
		{
			maxY = tbo.m_offsetY;
		}
	}
	return maxY + 1;
}

int TetrisShapeLayout::GetNumberOfBlocks()
{
	return 4;
}

int TetrisShapeLayout::GetNumberOfRotations()
{
	return m_layouts->size();
}
