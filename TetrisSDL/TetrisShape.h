#pragma once

#include <vector>
#include <SDL.h>
#include "TetrisBoard.h"
#include "TetrisShapeLayout.h"

class TetrisShape
{
public:
	TetrisShape(TetrisShapeType,TetrisBoard*);
	~TetrisShape();

	bool IsAlive();
	void MoveDown();

	void Rotate();
	void Update();
	void Render(SDL_Renderer *renderer);
	
private:
	int m_xPosition;
	int m_yPosition;

	int m_offsetX;
	int m_offsetY;

	int m_rotationIndex;
	bool m_alive;

	SDL_Color m_color;
	TetrisBoard *m_board;
	std::vector<TetrisBlock*> *m_blocks;
	TetrisShapeLayout *m_layout;
	
	void SetPosition(int x, int y);
	int GetPositionX();
	int GetPositionY();
};
