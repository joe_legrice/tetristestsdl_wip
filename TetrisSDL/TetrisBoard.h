#pragma once

#include <SDL.h>
#include "TetrisBlock.h"

class TetrisBoard
{
public:
	static const int c_width = 15;
	static const int c_height = 20;

	TetrisBoard();
	~TetrisBoard();

	void TryClearRows();
	bool IsValidPosition(unsigned int x, unsigned int y);
	void SetBlockAt(unsigned int x, unsigned int y, TetrisBlock* b);
	void Render(SDL_Renderer *renderer);

private:
	TetrisBlock* m_allBlocks[c_width * c_height];
};

