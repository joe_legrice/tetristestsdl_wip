#pragma once

#include <SDL.h>

class GameWindow
{
public:
	GameWindow(SDL_Window*);
	~GameWindow();

	static void SetWindowSize(int w, int h);

private:
	static SDL_Window* s_window;
};

