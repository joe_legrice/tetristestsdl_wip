#include "TetrisBlock.h"

TetrisBlock::TetrisBlock(SDL_Color color)
{
	m_color = color;
	m_xPosition = 0;
	m_yPosition = 0;
}

int TetrisBlock::GetPositionX()
{
	return m_xPosition;
}

int TetrisBlock::GetPositionY()
{
	return m_yPosition;
}

void TetrisBlock::SetPosition(int x, int y)
{
	m_xPosition = x;
	m_yPosition = y;
}

void TetrisBlock::Draw(SDL_Renderer *renderer)
{
	SDL_Rect rect;
	rect.x = m_xPosition * c_blockSize;
	rect.y = m_yPosition * c_blockSize;
	rect.w = c_blockSize;
	rect.h = c_blockSize;

	SDL_SetRenderDrawColor(renderer, m_color.r, m_color.b, m_color.g, m_color.a);
	SDL_RenderFillRect(renderer, &rect);

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderDrawRect(renderer, &rect);
}